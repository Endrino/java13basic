package firstweekpractice;

import java.util.Scanner;

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println("Входные данные: a = " + a + " b = " + b);
        a = a + b;
        b = a - b;
        a = a - b;

        System.out.println("Выходные данные: a = " + a + " b = " + b);

    }
}
