package firstweekpractice;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double x = scanner.nextDouble();
        double y = scanner.nextDouble();

        double d = Math.pow((x * x + y * y), 0.5);
        System.out.println(d);

    }

}
