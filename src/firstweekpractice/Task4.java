package firstweekpractice;

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double area = input.nextDouble();


        double diametr = Math.sqrt(area * 4 / Math.PI);
        double circum = diametr * Math.PI;

        System.out.println("Диаметр " + diametr + ", длина окружности " + circum);


    }
}
