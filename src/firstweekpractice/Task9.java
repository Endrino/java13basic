package firstweekpractice;

import java.util.Scanner;

public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a, b, c;
        a = scanner.nextInt();
        b = scanner.nextInt();
        c = scanner.nextInt();

        int discriminant = b * b - 4 * a * c;

        System.out.println(discriminant);
    }
}
