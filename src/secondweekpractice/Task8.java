package secondweekpractice;

import java.util.Scanner;

/*
Дан символ, поменять со строчного на заглавный или с заглавного на строчный

Входные данные
d
Выходные данные
D
Входные данные
A
Выходные данные
a
 */


public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String symbol = scanner.next();
        char c = symbol.charAt(0);
        if (c < 'A')
            c = (char)(c + ('A' - 'a'));
        else
            c = (char)(c - ('A' - 'a'));
        System.out.println(c);

//        if (c >= 'a' && c <= 'z'){
//            System.out.println((char) (c + ('A' - 'a')));
//        } else {
//            System.out.println((char) (c - ('A' - 'a')));
//        }

    }
}
