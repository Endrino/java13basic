package secondweekpractice;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        String str = null;

        if ((n % 2 == 0) && (n >= 0))
            str = " четное больше или равно 0";
        else if ((n % 2 == 0))
            str = " четное меньше 0";

        System.out.println(str);
    }
}
